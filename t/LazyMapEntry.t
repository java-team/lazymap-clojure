#(comment
exec("java", "clojure.lang.Script", $0) or die "Cannot exec Java!";
__END__
)
;-
; Copyright 2008,2009 (c) Meikel Brandmeyer.
; All rights reserved.
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in
; all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
; THE SOFTWARE.

(clojure.core/ns de.kotka.lazymap.test
  (:require
     [de.kotka.tap :as tap])
  (:import
     (de.kotka.lazymap LazyMapEntry)))

(tap/plan 5)

(def lme (new LazyMapEntry :foo (delay "Ok")))

(tap/is? (. lme getKey) :foo "getKey returns the key")
(tap/is? (. lme key) :foo "ditto for key")
(tap/is? (. lme getValue) "Ok" "getValue returns the Promised value")
(tap/is? (. lme val) "Ok" "ditto for val")
(tap/ok? (instance? clojure.lang.Delay (. lme getRawValue))
         "getRawValue actually returns the delayed value")

(. java.lang.System exit 0)
; vim:ft=clojure:
